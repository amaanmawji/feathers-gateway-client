const feathers = require('@feathersjs/feathers');
const rest = require('@feathersjs/rest-client');
const auth = require('@feathersjs/authentication-client');
const axios = require('axios');
const _ = require('lodash');

/**
 * Allows creation of feathers-clients which can call other services via our API gateway
 */
class FeathersGatewayClient {
  /**
   * 
   * @param options.app instance of feathers app
   * @param options.api target API
   */
  constructor(options) {
    this.app = options.app;
    this.baseUrl = options.app.get('APIGatewayBaseUrl');
    this.api = options.api;
  }

  get url() {
    return `${this.baseUrl}/${this.api}`;
  }

  /**
   * Creates a client that is authenticated with the target API via the provided token
   * @param {string} token JWT token for authenticating with the target API
   * @returns an authenticated instance of a feathers REST client
   */
  async withToken(token) {
    const client = feathers();
    const restClient = rest(this.url);
    client
      .configure(restClient.axios(axios))
      .configure(auth());

    client.set('accessToken', token);
    return client;
  }
  /**
   * Creates a client which can be used to impersonate a given user by creating a token with our shared secret
   * @param {string} userId the userId with which to do an "impersonation"
   * @returns a feathers-client authenticated/impersonating the userId provided
   */
  async asUser(userId) {
    const token = await this.app.passport.createJWT({
      userId,
    }, _.merge(this.app.get('authentication'), { jwt: { subject: userId.toString() } }));

    const client = feathers();
    const restClient = rest(this.url);
    client
      .configure(restClient.axios(axios))
      .configure(auth());

    client.set('accessToken', token);
    return client;
  }
  /**
   * Creates an anonymous/unauthenticated client
   * @returns a basic feathers client
   */
  async asAnonymous() {
    const client = feathers();
    const restClient = rest(this.url);
    client
      .configure(restClient.axios(axios));

    return client;
  }
};

module.exports = { FeathersGatewayClient };
