# feathers-gateway-client
A simple utility for dynamically creating feathers clients. Used for a pattern where independent feathers APIs are exposed via different URLs.  

## API Structure
This utility assumes you have a base URL, ex. `http://gateway:3030`, which exposes multiple separate feathers APIs such as:  
- `http://gateway:3030/api1`
- `http://gateway:3030/api2`
- `http://gateway:3030/api3`

## Usage
In your application config, set your API gateway base URL as `APIGatewayBaseUrl`
```javascript
const { FeathersGatewayClient } = require('feathers-gateway-client');

// api2 is an instance of FeathersGatewayClient
const api2 = new FeathersGatewayClient({ app, api: 'api2' });
// api2asUser2 is a feathers client authenticated as user with id: '2'
const api2asUser2 = await identity.asUser('2');

// You can now use api2asUser2 as you would use any other feathers client
console.log(await api2asUser2.service('todos').find({}));
```

## API
<a name="FeathersGatewayClient"></a>

## FeathersGatewayClient
Allows creation of feathers-clients which can call other services via our API gateway

**Kind**: global class  

* [FeathersGatewayClient](#FeathersGatewayClient)
    * [new FeathersGatewayClient()](#new_FeathersGatewayClient_new)
    * [.withToken(token)](#FeathersGatewayClient+withToken) ⇒
    * [.asUser(userId)](#FeathersGatewayClient+asUser) ⇒
    * [.asAnonymous()](#FeathersGatewayClient+asAnonymous) ⇒

<a name="new_FeathersGatewayClient_new"></a>

### new FeathersGatewayClient()

| Param | Description |
| --- | --- |
| options.app | instance of feathers app |
| options.api | target API |

<a name="FeathersGatewayClient+withToken"></a>

### feathersGatewayClient.withToken(token) ⇒
Creates a client that is authenticated with the target API via the provided token

**Kind**: instance method of [<code>FeathersGatewayClient</code>](#FeathersGatewayClient)  
**Returns**: an authenticated instance of a feathers REST client  

| Param | Type | Description |
| --- | --- | --- |
| token | <code>string</code> | JWT token for authenticating with the target API |

<a name="FeathersGatewayClient+asUser"></a>

### feathersGatewayClient.asUser(userId) ⇒
Creates a client which can be used to impersonate a given user by creating a token with our shared secret

**Kind**: instance method of [<code>FeathersGatewayClient</code>](#FeathersGatewayClient)  
**Returns**: a feathers-client authenticated/impersonating the userId provided  

| Param | Type | Description |
| --- | --- | --- |
| userId | <code>string</code> | the userId with which to do an "impersonation" |

<a name="FeathersGatewayClient+asAnonymous"></a>

### feathersGatewayClient.asAnonymous() ⇒
Creates an anonymous/unauthenticated client

**Kind**: instance method of [<code>FeathersGatewayClient</code>](#FeathersGatewayClient)  
**Returns**: a basic feathers client  
